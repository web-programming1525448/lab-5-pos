import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
    id: 1,
    email: 'mana12345@gmail.com',
    password: 'Pass@1234',
    fullName: 'มานะ งามดี',
    gender: 'female',
    roles: ['user']
})
  return { currentUser }
})
