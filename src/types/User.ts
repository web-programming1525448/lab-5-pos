type Gender = 'male' | 'female' | 'others'
type Roles = 'admin' | 'user'
type User = {
  id: number
  email: string
  password: string
  fullName: string
  gender: Gender
  roles: Roles[]
}

export type {Gender, Roles, User }